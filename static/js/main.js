var editor = 0;
var openfiles = [];
var files = {};
var editIndex = -1;
var EditSession = ace.require("ace/edit_session").EditSession;
var onSaveAction = (function() {});
function setupEditor() {
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.setReadOnly(true);
}

function setEditorLanguage(lang) {
    editor.getSession().setMode("ace/mode/"+lang);    
}

function postCommand(payload,callback) {
    url = '/cmd'
    var posting = $.post( url, { payload: JSON.stringify(payload) } ); 
    posting.done(callback);
}

function loadPath(path) {
    postCommand({
        'cmd': 'ls',
        'path': path
    }, function(data) {
        payload = JSON.parse(data)
        $('#dirpathpills').html(payload['pills']);
        $('#side').html(payload['side']);
    });
}

function currentFile() {
    return files[openfiles[editIndex]];    
}

function focusEdit(index) {
    if (editIndex > -1) {
        syncEdit();
    }
    editIndex = index;
    file = currentFile();
    editor.setSession(file['session']);
    syncTabs();
}

function dirtyFile(e) {
    if (editIndex == -1) return;
    //window.alert(e.type);

    files[openfiles[editIndex]]['dirty'] = true;
    syncTabs();
}

function newFileTab(file) {
    if (editIndex > -1) {
        syncEdit();
    }

    openfiles.push(file['path']);
    editIndex = openfiles.length-1;
    file['session'] = new EditSession("","ace/mode/text");
    file['dirty'] = false;
    editor.setSession(file['session']);
    setEditorLanguage(file['langkey']);
    editor.setValue(file['contents']);
    editor.clearSelection();
    editor.moveCursorTo(0,0);
    editor.setReadOnly(false);
    editor.getSession().on('change', dirtyFile);    
    files[file['path']] = file;
    syncEdit();
    syncTabs();
}

function syncEdit() {
    //f = currentFile();
    //path = f['path'];
    //files[path]['session'] = editor.getSession();
}

function syncTabs() {
    headActive = '<li class="active">';
    head = '<li>';
    tail = '</li>';
    html = "";
    for (i = 0; i < openfiles.length; i++) {
        file = files[openfiles[i]];
        tabname = file['shortname'];
        if (file['dirty']) tabname += "*";
        link = '<a href="#" onClick="focusEdit(' + i + ');">' + tabname + '</a>';
        if (i == editIndex) {
            html += headActive;
        }
        else {
            html += head;
        }
        html += link + '</a>' + tail;
    }

 //   window.alert(html);
    if (openfiles.length > 0) {
        $('#tablist').html(html);
        file = currentFile();
        $('#filetitle').html('/'+file['path']);
        $('#languagetitle').html(file['lang']['name']);
        $('[data-group="openFileActions"][data-wtype="button"]').show(100);
        $('[data-group="openFileActions"][data-wtype="menu"]').removeClass('disabled');
    }
    else {
        $('#tablist').html('<li class="disabled"><a href="#">No Files Open</a></li>');
        $('#filetitle').html('Please open a file.');
        $('#languagetitle').html('&nbsp;');
        $('[data-group="openFileActions"][data-wtype="button"]').hide(100);
        $('[data-group="openFileActions"][data-wtype="menu"]').addClass('disabled');
    }
}

function isOpen(file) {
    return openfiles.indexOf(file['path']) != -1;
}

function indexOfFile(file) {
    return openfiles.indexOf(file['path']);    
}

function showSaveConfirmation() {
    options = {
        'backdrop': true,
        'keyboard': true
    }
    $('#confirmSaveModal').modal(options);
}
function showSaveModal() {
    options = {
        'backdrop': 'static',
        'keyboard': false
    }
    $('#savingModal').modal(options);
}
function closeSaveModal() {
    $('#savingModal').modal('hide');
}

function saveAndClose() {
    $('#confirmSaveModal').modal('hide');
    onSaveAction = forceClose;
    saveFile();
}

function cancelClose() {
    $('#confirmSaveModal').modal('hide');
    // do nothing
}

function cancelSave() {
    $('#saveErrorModal').modal('hide');
    // do nothing
}

function showSaveError() {
        options = {
        'backdrop': true,
        'keyboard': true
    }
    $('#saveErrorModal').modal(options);

}

function trySavingAgain() {
    $('#saveErrorModal').modal('hide');
    saveFile();
}

function saveFile() {
    file = currentFile();
    showSaveModal();
    postCommand({
        'cmd': 'save',
        'path': file['path'],
        'contents': editor.getValue()
    }, function(data) {
        closeSaveModal()
        payload = JSON.parse(data)
        if (payload['success']) {
            files[openfiles[editIndex]]['dirty'] = false;
            syncTabs();
            action = onSaveAction;
            onSaveAction = (function() {});
            action();
        }
        else {
            onSaveAction = (function() {});
            showSaveError();
        }
    });


}
function blankEditor() {
    editor.setSession(new EditSession("","ace/mode/text"));
    editor.setReadOnly(true);
}

function notImplemented() {
    window.alert("Not Implemented");    
}

function newFile() {
    notImplemented();
}

function saveAsFile() {
    notImplemented();
}


function forceClose() {
    $('#confirmSaveModal').modal('hide');
    delete files[openfiles[editIndex]];
    openfiles.splice(editIndex,1);
    min = 0;
    max = openfiles.length - 1;
    newIndex = editIndex-1;
    if (newIndex < min) newIndex = min;
    if (newIndex > max) {
        editIndex = -1;
        blankEditor();
        syncTabs();
    }
    editIndex = -1;
    focusEdit(newIndex);
}

function closeFile() {
    file = currentFile();
    if (file['dirty']) showSaveConfirmation();
    else {
        forceClose();
    }

}

function editPath(path) {
    postCommand({
        'cmd': 'edit',
        'path': path
    }, function(data) {
        payload = JSON.parse(data)
        if (payload['success']) {
            if (isOpen(payload)) {
                focusEdit(indexOfFile(payload));
            }
            else {
                newFileTab(payload);
            }

        }
    });
}


function init() {
    setupEditor();
    loadPath('');
    syncTabs();
}
$(init);
