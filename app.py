from flask import Flask, url_for, redirect, request
import mint
import os
import json

#globals TODO - move these out of globals...
cfg = {}
languages = {}
debug = False

symbols = {
    'copy': mint.Markup("&copy;"),
    'times': mint.Markup("&times;")
}

app = Flask(__name__)

loader = mint.Loader('./layout', cache=False)

baseTemplate = ''
importTemplates = []
stylesheets = []
scripts = []
menus = []

html5 = mint.Markup("<!DOCTYPE html>")

def js(f='main'):
    url = url_for('static', filename='js/%s.js' % f)
    return url
def css(f='style'):
    return url_for('static', filename='css/%s.css' % f)
def img(f='logo',x='png'):
    return url_for('static', filename='img/%s.%s' % (f,s))


def loadConfig():
    global cfg
    if os.path.exists("config.json"):
        with open("config.json","r") as cf:
            cfg = json.loads(cf.read())
    with open("defaults.json","r") as cf:
        defaults = json.loads(cf.read())
        for k in defaults.keys():
            if k not in cfg:
                cfg[k] = defaults[k]

    cfg['root'] = os.path.expanduser(cfg['root'].encode('utf8'))

def loadLanguages():
    global languages
    if os.path.exists("languages.json"):
        with open("languages.json","r") as cf:
            languages = json.loads(cf.read())

def loadLayout():
    global baseTemplate,importTemplates,stylesheets,scripts,menus
    tmp = {}
    if os.path.exists("layout/templates.json"):
        with open("layout/templates.json","r") as cf:
            tmp = json.loads(cf.read())
    baseTemplate = tmp['baseTemplate']
    importTemplates = tmp['importTemplates']

    if os.path.exists("layout/stylesheets.json"):
        with open("layout/stylesheets.json","r") as cf:
            stylesheets = json.loads(cf.read())

    if os.path.exists("layout/scripts.json"):
        with open("layout/scripts.json","r") as cf:
            scripts = json.loads(cf.read())

    if os.path.exists("layout/menu.json"):
        with open("layout/menu.json","r") as cf:
            menus = json.loads(cf.read())


def resolveStylesheets():
    arr = []
    for x in stylesheets:
        if x.startswith('http:') or x.startswith('https:'):
            arr.append(x)
        else:
            arr.append(css(x))
    return arr

def resolveScripts():
    arr = []
    for x in scripts:
        if x.startswith('http:') or x.startswith('https:'):
            arr.append(x)
        else:
            arr.append(js(x))
    return arr

def jsf(func,params=""):
    return '%s(%s)' % (func,params);

def jsSaveFile():
    return 'saveFile();'
def jsCloseFile():
    return 'closeFile();'
def jsLoadPath(path):
    return 'loadPath(\'%s\');' % path
def jsEditPath(path):
    return 'editPath(\'%s\');' % path

def getdirs(path):
    dirs = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path,f))]
    return sorted(dirs, key=str.lower)

def getfiles(path):
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f))]
    return sorted(files, key=str.lower)


def splitpath(path, maxdepth=20):
     ( head, tail ) = os.path.split(path)
     return splitpath(head, maxdepth - 1) + [ tail ] \
         if maxdepth and head and head != path \
         else [ head or tail ]

def parentpath(path):
    print "CWD: %s" % path
    if path[-1] == '/':
        path = path[0:len(path)-1]
    if '/' not in path:
        return ''
    return os.path.split(path)[0]

def pathencode(path,action='view'):
    return ('/%s/' % action) + path.replace('/','$')

def pathdecode(path):
    return path.replace('$','/')
def generateScope():
    return {
        'title': cfg['title'],
        'stylesheets': resolveStylesheets(),
        'scripts': resolveScripts(),
        'menus': menus,
        'html5': html5,
        'js': js,
        'css': css,
        'pathjoin': os.path.join,
        'pathencode': pathencode,
        'symbols': symbols,
        'nbsp': mint.Markup('&nbsp;'),
        'loadPath': jsLoadPath,
        'editPath': jsEditPath,
        'saveFile': jsSaveFile,
        'closeFile': jsCloseFile,
        'jsf': jsf
    }

#@scribble.command("ls")
def lsCommand(data):
    workdir = data['path']
    cwd = workdir.encode('utf8')
    scope = generateScope()
    scope['root'] = cfg['root']
    scope['cwd'] = cwd
    pp = splitpath(cwd)
    if pp in ['..','.']:
        return getSidebarMarkup('')
    pathparts = []
    for i,x in enumerate(pp):
        path = os.path.join(*pp[0:i+1])
        pathparts.append({'f':x,'path':path})
    scope['pathparts'] = pathparts
    abspath = os.path.join(cfg['root'],cwd)
    dirs = getdirs(abspath)
    dirpaths = []
    if cwd != '':
        dirpaths.append({'f': '..','path': parentpath(cwd).decode('utf8')})
    for f in dirs:
        dirpaths.append({'f': f.decode('utf8'), 'path': os.path.join(cwd,f).decode('utf8')})

    scope['dirs'] = dirpaths

    files = getfiles(abspath)
    filepaths = []
    for f in files:
        filepaths.append({'f': f.decode('utf8'), 'path': os.path.join(cwd,f).decode('utf8')})

    scope['files'] = filepaths
    return {
        'pills': getTemplate('fragment/breadcrumb').render(**scope),
        'side': getTemplate('fragment/sidebar').render(**scope)
    }

#@scribble.command("save")
def saveCommand(data):
    workfile = data['path']
    contents = data['contents']
    parts = os.path.split(workfile)
    for pp in splitpath(parts[0]): #hack detection
        if pp in ['..','.']:
            return {
                'success': False,
                'message': "Invalid path."
            }
    cwd = parts[0]
    cfile = parts[1]
    absfile = os.path.join(cfg['root'],cwd,cfile)
    message = 'File created.'
    if os.path.exists(absfile):
        message = 'File saved.'
    if cfile != '':
        with open(absfile,'w') as fl:
            fl.write(contents)


    payload =  {
        'success': True
    }
    return payload

#@scribble.command("stat")
def statCommand(data):
    workfile = data['path']
    parts = os.path.split(workfile)
    for pp in splitpath(parts[0]): #hack detection
        if pp in ['..','.']:
            print "HAX!"
            return {
                'success': False,
                'message': "Invalid path."
            }
    cwd = parts[0]
    cfile = parts[1]
    absfile = os.path.join(cfg['root'],cwd,cfile)
    if os.path.exists(absfile):
        # do stuff
        x = 0
        return {
            'success': True,
            'shortname': cfile,
            'path': workfile
        }

    return {
        'success': False,
        'shortname': cfile,
        'path': workfile,
        'message': 'File not found'
    }

#@scribble.command("edit")
def editCommand(data):
    workfile = data['path']
    parts = os.path.split(workfile)
    for pp in splitpath(parts[0]): #hack detection
        if pp in ['..','.']:
            print "HAX!"
            return {
                'success': False,
                'message': "Invalid path."
            }
    cwd = parts[0]
    cfile = parts[1]
    text = ''
    absfile = os.path.join(cfg['root'],cwd,cfile)
    if cfile != '':
        with open(absfile,'r') as fl:
            text = fl.read()
    else:
        text = None

    ext = os.path.splitext(cfile)[1][1:]
    print ext

    langkey = 'text'
    for lk in languages.keys():
        lang = languages[lk]
        if ext in lang['ext']:
            langkey = lk
            break
    lang = languages[langkey]
    payload =  {
        'success': True,
        'contents': text,
        'langkey': langkey,
        'lang': lang,
        'shortname': cfile,
        'path': workfile
    }
    print lang 
    print langkey
    return payload




#TODO - make this a decorator based system
commandMap = {
    'ls': lsCommand,
    'edit': editCommand,
    'save': saveCommand,
    'stat': statCommand
}

@app.route("/cmd",methods=["POST"])
def processcmd():
    payload = json.loads(request.form['payload'])
    print payload
    cmd = payload['cmd']
    if cmd in commandMap:
        return json.dumps(commandMap[cmd](payload))


def getTemplate(name):
    return loader.get_template(name+".mint")

@app.route("/")
def approot():
    scope = generateScope()
    for temp in importTemplates:
        frag = mint.Markup(getTemplate(temp).render(**scope))
        scope[temp.replace('/','_')] = frag
    return getTemplate(baseTemplate).render(**scope)
    
if __name__ == "__main__":
    loadConfig()
    loadLanguages()
    loadLayout()
    app.run(host=cfg['host'],port=cfg['port'],debug=cfg['debug'])