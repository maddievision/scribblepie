@ul.class()
    #for item in dirs:
        @li
            @a.href(#).onClick({{ loadPath(item['path']) }}) 
                @i.class(icon-folder-open icon-white)
                {{ nbsp }}
                {{ item['f']}}
    #for item in files:
        @li
            @a.href(#).onClick({{ editPath(item['path']) }})
                @i.class(icon-file icon-white)
                {{ nbsp }}
                {{ item['f'] }}