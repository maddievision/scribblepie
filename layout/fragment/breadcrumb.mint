#if cwd == '':
    @li.class(active) 
        @a.href(#).onClick({{ loadPath('') }}) / 
#else:
    @li
        @a.href(#).onClick({{ loadPath('') }}) / 
    #for p in pathparts[0:len(pathparts)-1]
        @li
            @a.href(#).onClick({{ loadPath(p['path']) }}) {{ p['f'] }}
    @li.class(active) 
        @a.href(#).onClick({{ loadPath(pathparts[-1]['path']) }}) {{ pathparts[-1]['f'] }}
