#def loadStylesheets():
    #for css in stylesheets:
        @link.rel(stylesheet).href({{ css }})

#def loadScripts():
    #for script in scripts:
        @script.src({{ script }})

{{ html5 }}
@html.lang(en)
    @head
        @title {{ title }}

        #loadStylesheets()
        #loadScripts()

    @body
        {{ modal_confirmSaveModal }}
        {{ modal_savingModal }}
        {{ modal_saveErrorModal }}
        {{ navbar }}

        @div.id(top-wrap)
            @div.id(path-wrap)                    
                @ul.class(nav nav-pills).id(dirpathpills)
                    @li.class(active) 
                        @a.href(#) / 
                    @li.class(disabled) 
                        @a.href(#) Loading...
            @div.id(file-tab-wrap)
                @ul.id(tablist)
                    @li.class(disabled)
                        @a.href(#) No Files Open
                @div.id(tab-separator)
        @div.id(main-wrap)
            @div.id(sidebar-wrap)
                @div.id(side)
                    @div.id(sideprogress).class(progress progress-striped active)
                        @div.class(bar).style(width: 100%)
            @div.id(editor-wrap)
                @div.id(editor)
                @div.id(editprogress).class(ehidden progress progress-striped active)
                    @div.class(bar).style(width: 100%)