@div.class(modal hide fade).id(saveErrorModal)
    @div.class(modal-header)
        @h4 Error
    @div.class(modal-body)
        @p.class(text-error) The file could not be saved.
    @div.class(modal-footer)
        @a.href(#).class(btn).onClick({{ jsf('cancelSave') }}) Go Back to Editing
        @a.href(#).class(btn).onClick({{ jsf('trySavingAgain') }}) Try Again