@div.class(modal hide fade).id(confirmSaveModal)
    @div.class(modal-header)
        @button.type(button).class(close).data-dismiss(modal).aria-hidden(true) {{ symbols['times'] }}
        @h4 Confirm Close
    @div.class(modal-body)
        @p This file has been changed.  What would you like to do?
    @div.class(modal-footer)
        @a.href(#).class(btn).onClick({{ jsf('cancelClose') }}) Don't Close, Keep Editing
        @a.href(#).class(btn btn-danger).onClick({{ jsf('forceClose') }}) Don't Save, Just Close
        @a.href(#).class(btn btn-primary).onClick({{ jsf('saveAndClose') }}) Save and Close