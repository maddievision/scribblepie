@div.class(modal hide fade).id(savingModal)
    @div.class(modal-header)
        @h4 Saving...
    @div.class(modal-body)
        @div.id(sideprogress).class(progress progress-striped active)
            @div.class(bar).style(width: 100%)