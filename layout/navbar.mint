#def printMenuTitle(menu):
    #if 'icon' in menu:
        @i.class({{ menu['icon'] }})
        {{ nbsp }}
    {{ menu['title'] }}

#def parseMenuItem(menu,level=0):
    #if menu['id'] == 'sep':
        @li.class(divider).data-group({{ menu['group'] if 'group' in menu else '' }}).data-wtype(menu)
    #elif 'items' in menu:
        #if level == 0:
            @li.class(dropdown).id({{ menu['id'] }}).data-group({{ menu['group'] if 'group' in menu else '' }}).data-wtype(menu)
                @a.href(#).class(dropdown-toggle).data-toggle(dropdown)
                    #printMenuTitle(menu)
                    @b.class(caret)
                @ul.class(dropdown-menu)
                    #for subitem in menu['items']:
                        #parseMenuItem(subitem,level+1)
        #else:
            @li.class(dropdown-submenu).id({{ menu['id'] }}).data-group({{ menu['group'] if 'group' in menu else '' }}).data-wtype(menu)
                @a.href(#)
                    #printMenuTitle(menu)
                @ul.class(dropdown-menu)
                    #for subitem in menu['items']:
                        #parseMenuItem(subitem,level+1)

    #else:
        @li.id({{ menu['id'] }}).data-group({{ menu['group'] if 'group' in menu else '' }}).data-wtype(menu)
            @a.href(#).onClick({{ jsf(menu['action']) if 'action' in menu else ''}})
                #printMenuTitle(menu)

@div.class(navbar)
    @div.class(navbar-inner)
        @a.class(brand).href(#) {{ title }}
        @ul.class(nav)
            #for menu in menus:
                #parseMenuItem(menu)
        @ul.class(nav pull-right)
            @li
                @a.href(#).id(languagetitle) {{ nbsp }}
            @li.class(active)
                @a.href(#).id(filetitle) Loading...
            @li.data-group(openFileActions).data-wtype(button)
                @a.href(#).onClick({{ closeFile() }}) 
                    @i.class(icon-remove)